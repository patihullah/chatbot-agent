from typing import List
from fastapi import WebSocket

"""
class Connection Manager to handle Disconenction and Multiple clients
"""


class ConnectionManager:
    def __init__(self):
        self.active_connection: List[WebSocket] = []

    async def connect(self, websocket: WebSocket):
        await websocket.accept()
        self.active_connection.append(websocket)

    def disconnect(self, websocket: WebSocket):
        self.active_connection.remove(websocket)

    @staticmethod
    async def send_personal_message(message: {}, websocket: WebSocket):
        await websocket.send_json(message)

    async def broadcast(self, message: {}):
        for connection in self.active_connection:
            await connection.send_json(message)
