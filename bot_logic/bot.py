from chatterbot import ChatBot
from chatterbot.trainers import ListTrainer


class AgentBot:
    def __init__(self):
        self.__chatbot = ChatBot("Anbot")
        self.__train()

    def __train(self):
        trainer = ListTrainer(self.__chatbot)
        trainer.train(
            [
                "Hello",
                "Hello how are you",
                "I am doing well thanks" "So what do you like to do for fun?",
                "Code",
                "Oh really?, I have a great app idea if you want to hear it?",
                "No I dont",
                "What is your favorite type of keyboard",
                "Split",
                "Why is that?",
                "Because they are more ergonomic",
            ]
        )

    def chat(self, msg: str):
        if msg != "":
            return self.__chatbot.get_response(msg)
        else:
            return "Send me some text to start the conversation"


if __name__ == "__main__":
    bot = AgentBot()
    print(bot.chat(""))
    print(bot.chat("Hello"))
