# chatbot-agent

this simple agent bot for conversation

Stack Tech:

1. Back-end using FastAPI
2. Front-end using ReactJS
3. bot using Chatterbot

first you must install package, using command

`pip install -r requirements.txt`

for running, use command

`python main.py` or `uvicorn main:app`