import uvicorn
from fastapi import FastAPI, WebSocket, WebSocketDisconnect
from utils.connection_ws import ConnectionManager
from bot_logic.bot import AgentBot

app = FastAPI()
api_bot = AgentBot()
manager = ConnectionManager()


@app.get("/api/v.01/")
async def home():
    return {"Hello": "World"}


@app.websocket('/api/v.01/ws/{client_id}')
async def websocket_endpoint(websocket: WebSocket, client_id: int):
    await manager.connect(websocket)
    data = {}
    try:
        dchat_start = {
            "username": "Bot",
            "content": "Hi greetings, I'm Anbot who will help you",
            "img": "https://icon-library.com/images/customer-service-icon-png/customer-service-icon-png-27.jpg",
        }

        bchat_unknown = {
            "username": "Bot",
            "content": "Sorry, I do not know",
            "img": "https://icon-library.com/images/customer-service-icon-png/customer-service-icon-png-27.jpg",
        }

        await manager.send_personal_message(dchat_start, websocket)
        while True:
            data = await websocket.receive_json()
            data["img"] = ""
            await manager.send_personal_message(data, websocket)

            msg = api_bot.chat(data['content'])
            bchat_unknown['content'] = str(msg)
            await manager.send_personal_message(bchat_unknown, websocket)
            # await manager.broadcast(bchat_unknown)

    except WebSocketDisconnect:
        manager.disconnect(websocket)
        data['content'] = "client #{} left the chart".format(client_id)
        await manager.broadcast(data)


if __name__ == "__main__":
    uvicorn.run("main:app")
